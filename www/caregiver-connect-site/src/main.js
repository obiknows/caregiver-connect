import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

// libs
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
Vue.use(Buefy)

// App
import App from './App.vue'

import Home from './components/Home/Home'
import About from './components/About/About'
import Contact from "./components/Contact/Contact";
import Survey from './components/Survey/Survey'
import Resources from "./components/Resources/Resources";
import Forum from "./components/Forum/Forum";
import Welcome from './components/Welcome'
import Finished from "./components/Finished";

// const hellosign = window.HelloSign;
// const CLIENT_ID =
//   "892ed24f285917031a1be78a33f7230cd4160c6fc414e5878fb8766723d81307";

// hellosign.init(CLIENT_ID);

const router = new VueRouter({
  // mode: "history",
  // base: __dirname,
  routes: [
    {
      path: "/",
      name: "Entry",
      component: Welcome,
      props: { isEntry: true }
    },
    {
      path: "/home",
      name: "Home",
      component: Home
    },
    {
      path: "/survey",
      name: "Survey",
      component: Survey,
      props: route => ({ param: route.query.q })
    },
    { path: "/resources", component: Resources },
    { path: "/finished", component: Finished },
    { path: "/about", component: About },
    { path: "/contact", component: Contact },
    { path: "/forum", component: Forum },
    // { path: "/dashboard", component: Dashboard, beforeEnter: requireAuth },
    // { path: "/login", component: Login },
    {
      path: "/logout",
      beforeEnter(to, from, next) {
        // auth.logout();
        next("/");
      }
    }
  ]
});


Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
