caregiver connect
=================

a represent™️ site  

### how to use

basically we use *Docker Compose*

```
# to build
$ docker-compose -f docker-compose.yml up -d --build
```
```
# to stop
$ docker-compose -f docker-compose.yml down
```
```
# to restart
docker-compose -f docker-compose.yml down
docker-compose -f docker-compose.yml up -d --build
```